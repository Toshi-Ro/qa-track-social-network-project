/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 98.45360824742268, "KoPercent": 1.5463917525773196};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8530927835051546, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.9666666666666667, 500, 1500, "Go to Latest Posts page"], "isController": false}, {"data": [1.0, 500, 1500, "Go to Login page"], "isController": false}, {"data": [0.9666666666666667, 500, 1500, "Go to Users page"], "isController": false}, {"data": [0.9666666666666667, 500, 1500, "Log out"], "isController": false}, {"data": [0.8333333333333334, 500, 1500, "Go to a specific post"], "isController": false}, {"data": [1.0, 500, 1500, "Go to About Us page"], "isController": false}, {"data": [1.0, 500, 1500, "Log out-1"], "isController": false}, {"data": [0.2857142857142857, 500, 1500, "Go to Home page"], "isController": false}, {"data": [1.0, 500, 1500, "Log out-0"], "isController": false}, {"data": [0.5, 500, 1500, "Log into user\'s account"], "isController": false}, {"data": [1.0, 500, 1500, "Go to a specific user"], "isController": false}, {"data": [0.5333333333333333, 500, 1500, "Log into user\'s account-0"], "isController": false}, {"data": [1.0, 500, 1500, "Log into user\'s account-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 194, 3, 1.5463917525773196, 437.3608247422683, 136, 1621, 292.0, 1060.5, 1267.0, 1558.3000000000006, 37.05118411000764, 286.40642158852177, 8.30600350935829], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["Go to Latest Posts page", 15, 0, 0.0, 299.4666666666667, 168, 534, 273.0, 503.40000000000003, 534.0, 534.0, 10.989010989010989, 216.93638392857142, 2.0819024725274726], "isController": false}, {"data": ["Go to Login page", 15, 0, 0.0, 285.13333333333327, 186, 351, 287.0, 340.8, 351.0, 351.0, 36.85503685503686, 207.5255297911548, 5.074765816953318], "isController": false}, {"data": ["Go to Users page", 15, 0, 0.0, 302.26666666666665, 213, 675, 264.0, 548.4000000000001, 675.0, 675.0, 9.621552277100706, 70.87438361930725, 1.8228331462475946], "isController": false}, {"data": ["Log out", 15, 0, 0.0, 344.73333333333335, 305, 501, 328.0, 424.80000000000007, 501.0, 501.0, 8.83912787271656, 53.59584468915733, 3.4182564820271066], "isController": false}, {"data": ["Go to a specific post", 15, 0, 0.0, 342.93333333333334, 187, 637, 292.0, 578.8000000000001, 637.0, 637.0, 10.344827586206897, 111.27761314655173, 2.000269396551724], "isController": false}, {"data": ["Go to About Us page", 15, 0, 0.0, 153.13333333333333, 141, 167, 151.0, 166.4, 167.0, 167.0, 9.633911368015413, 75.88586825626204, 1.8251746146435452], "isController": false}, {"data": ["Log out-1", 15, 0, 0.0, 186.33333333333337, 163, 230, 178.0, 220.4, 230.0, 230.0, 10.518934081346423, 59.23063858695652, 2.064751709326788], "isController": false}, {"data": ["Go to Home page", 14, 3, 21.428571428571427, 1240.7857142857142, 719, 1621, 1266.5, 1588.0, 1621.0, 1621.0, 8.636644046884639, 61.949365746452806, 1.147054287476866], "isController": false}, {"data": ["Log out-0", 15, 0, 0.0, 157.53333333333336, 136, 269, 147.0, 221.60000000000002, 269.0, 269.0, 9.784735812133071, 4.233044887475538, 1.8633041829745598], "isController": false}, {"data": ["Log into user\'s account", 15, 0, 0.0, 1054.7333333333333, 588, 1474, 1015.0, 1420.6000000000001, 1474.0, 1474.0, 9.287925696594426, 95.01983359133126, 4.226731811145511], "isController": false}, {"data": ["Go to a specific user", 15, 0, 0.0, 318.3999999999999, 260, 462, 310.0, 410.40000000000003, 462.0, 462.0, 8.710801393728223, 81.18739111498257, 1.6758084712543555], "isController": false}, {"data": ["Log into user\'s account-0", 15, 0, 0.0, 793.7333333333333, 322, 1190, 724.0, 1162.4, 1190.0, 1190.0, 11.252813203300825, 5.64838475243811, 3.043973884096024], "isController": false}, {"data": ["Log into user\'s account-1", 15, 0, 0.0, 260.06666666666666, 181, 350, 251.0, 331.40000000000003, 350.0, 350.0, 13.181019332161688, 128.23175252636204, 2.432824857205624], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["500", 3, 100.0, 1.5463917525773196], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 194, 3, "500", 3, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Go to Home page", 14, 3, "500", 3, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
