package com.telerikacademy.finalproject.pages;

import org.openqa.selenium.Keys;

public class CategoriesPage extends BasePage{

    public CategoriesPage() {
        super("categories.url");
    }

    public void navigateToCategoriesPage() {
        actions.hoverElement("navigation.SettingsSection");
        actions.isElementPresentUntilTimeout("navigation.CategoriesSection", 2000);
        actions.clickElement("navigation.CategoriesSection");
    }

    public void clickAddCategoryButton() {
        actions.clickElement("button.Add");
    }

    public void clickEditCategoryButton() {
        actions.clickElement("button.EditCategory");
    }

    public void populateNewCategoryFields() {
        actions.typeValueInField(filePath + "apple.jpg", "button.UploadPicture");
        actions.typeValueInField("Seasonal fruits", "categories.CategoryNameField");
        actions.clickElement("button.Submit");
    }

    public void updateCategoryFields() {
        actions.typeValueInField(filePath + "lemon.jpg", "button.UploadPicture");
        actions.clearElement("categories.CategoryNameField");
        actions.typeValueInField("Exotic fruits", "categories.CategoryNameField");
        actions.clickElement("button.Submit");
    }

    public void deleteCategory() {
        actions.clickElement("button.EditUpdatedCategory");
        actions.clickElement("button.Delete");
        actions.isElementPresentUntilTimeout("button.Submit", 5000);
        actions.hoverElement("button.Submit");
        actions.clickElement("button.Submit");
    }

    //--------------ASSERTS-----------------
    public void assertCategoriesPageNavigated() {
        assertSpecificPageNavigated("categories.url");
    }

    public void assertNewCategoryPageNavigated() {
        assertSpecificPageNavigated("categories.new");
    }

    public void assertNewCategoryCreated() {
        actions.isElementPresentUntilTimeout("categories.CreatedCategoryLocator", 5000);
        actions.assertElementPresent("categories.CreatedCategoryLocator");
        //END key for visibility
        actions.pressKey(Keys.END);
    }

    public void assertCategoryIsUpdated() {
        actions.isElementPresentUntilTimeout("categories.UpdatedCategoryLocator", 5000);
        actions.assertElementPresent("categories.UpdatedCategoryLocator");
        //END key for visibility
        actions.pressKey(Keys.END);
        actions.waitFor(1000);
    }

    public void assertCategoryNotPresent() {
        actions.assertElementNotPresent("categories.AssertDeleteCategory");
    }
}