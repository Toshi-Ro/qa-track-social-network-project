package com.telerikacademy.finalproject.pages;

public class UsersPage extends BasePage{

    public UsersPage() {
        super("users.url");
    }

    public void navigateToUsersPage() {
        actions.hoverElement("navigation.UsersSection");
        actions.clickElement("navigation.UsersSection");
    }

    public void navigateToUserRequestsPage() {
        actions.hoverElement("navigation.UsersSection");
        actions.clickElement("navigation.UsersRequestsSection");
    }

    public void navigateToUserOneProfile() {
        actions.navigateToUrl("connections.url");
        actions.isElementPresentUntilTimeout("users.User1Profile", 5000);
        actions.clickElement("users.User1Profile");
    }

    public void sendConnectionRequest() {
        actions.isElementPresentUntilTimeout("users.User2Profile", 5000);
        actions.clickElement("users.User2Profile");
        actions.assertElementPresent("users.User2EmailValidation");
        actions.clickElement("button.Submit");
    }

    public void confirmConnection() {
        actions.clickElement("users.User1Profile");
        actions.isElementPresentUntilTimeout("button.ConfirmConnection", 5000);
        actions.clickElement("button.ConfirmConnection");
    }

    public void clickOnDisconnectButton() {
        actions.clickElement("button.Submit");
    }

    //--------------ASSERTS-----------------
    public void assertUsersPageNavigated() {
        assertSpecificPageNavigated("users.url");
    }

    public void assertConnectionRequestIsSent() {
        actions.isElementPresentUntilTimeout("button.SentRequest", 5000);
        actions.assertElementPresent("button.SentRequest");
    }

    public void assertRequestIsReceived() {
        actions.isElementPresentUntilTimeout("users.User1ConnectionRequest", 5000);
        actions.assertElementPresent("users.User1ConnectionRequest");
    }

    public void assertConnectionIsConfirmed() {
        actions.assertElementPresent("button.Submit");
    }

    public void assertDisconnectedFromUser() {
        actions.assertElementPresent("button.Submit");
    }
}
