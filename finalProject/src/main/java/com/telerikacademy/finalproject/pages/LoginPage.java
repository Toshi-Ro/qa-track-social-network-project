package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

public class LoginPage extends BasePage {

    public LoginPage() {
        super("login.url");
    }

    public final String loginButton = "navigation.LoginSection";

    public void navigateToLoginPage() {
        actions.clickElement(loginButton);
    }

    public void logIn(String username, String password) {
        actions.typeValueInField(Utils.getConfigPropertyByKey(username),"login.UsernameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey(password), "login.PasswordField");
        actions.clickElement("button.Submit");
    }

    //--------------ASSERTS-----------------
    public void assertLoginPageNavigated() {
        assertPageNavigated();
    }

    public void assertUserIsLoggedIn() {
        actions.assertElementPresent("button.LogOut");
    }

}
