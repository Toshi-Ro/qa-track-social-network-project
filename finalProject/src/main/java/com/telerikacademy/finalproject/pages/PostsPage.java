package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;

public class PostsPage extends BasePage{

    public PostsPage() {
        super("posts.url");
    }

    public void navigateToLatestPostsPage() {
        actions.clickElement("navigation.LatestPostsSection");
    }

    public void navigateToNewPostPage() {
        actions.hoverElement("navigation.LatestPostsSection");
        actions.clickElement("button.NewPost");
    }

    public void navigateToCreatedPostFromUserProfile() {
        actions.clickElement("posts.CreatedPostLocator");
    }


    public void createPost() {
        actions.typeValueInField(filePath + "apple.jpg", "button.UploadPicture");
        actions.typeValueInField("Apple", "posts.TitleField");
        actions.clickElement("posts.InitialCategory");
        actions.typeValueInField("Apples are good for your health.", "posts.DescriptionField");
        actions.clickElement("button.Submit");
    }

    public void openPostInLatestPostsPage() {
        actions.clickElement("posts.CreatedPostLocatorInLatestPosts");
    }

    public void likePost() {
        actions.clickElement("button.LikePost");
        actions.assertElementPresent("button.DislikePost");
        //wait for visibility
        actions.waitFor(1500);
    }

    public void dislikePost() {
        actions.clickElement("button.DislikePost");
        actions.assertElementPresent("button.LikePost");
    }

    public void updateAllPostFields() {
        actions.clickElement("button.EditPost");
        actions.typeValueInField(filePath + "lemon.jpg", "button.UploadPicture");
        actions.clearElement("posts.TitleField");
        actions.typeValueInField("Lemon", "posts.TitleField");
        actions.clickElement("posts.InitialCategory");
        actions.clickElement("posts.UpdatedCategory");
        actions.clearElement("posts.DescriptionField");
        actions.typeValueInField("Lemons make lemonade.", "posts.DescriptionField");
        actions.clickElement("button.Submit");
    }

    public void searchForPost(String searchWord) {
        actions.typeValueInField(Utils.getConfigPropertyByKey(searchWord), "posts.SearchField");
        actions.hoverElement("button.SearchPost");
        actions.clickElement("button.SearchPost");
        actions.waitFor(1000);
    }

    public void deletePost() {
        actions.clickElement("button.EditPost");
        actions.clickElement("button.Delete");
        actions.isElementPresentUntilTimeout("button.Submit", 5000);
        actions.hoverElement("button.Submit");
        actions.clickElement("button.Submit");
    }

    public void addComment() {
        actions.typeValueInField("This is a test comment.", "comments.Field");
        actions.clickElement("button.Submit");
    }

    public void editComment() {
        actions.clickElement("button.EditComment");
    }

    public void updateComment() {
        actions.clearElement("comments.Field");
        actions.typeValueInField("This is the new text for our updated comment.", "comments.Field");
        actions.hoverElement("button.Submit");
        actions.clickElement("button.Submit");
    }

    public void likeComment() {
        actions.clickElement("button.LikeComment");
    }

    public void dislikeComment() {
        actions.clickElement("button.DislikeComment");
    }

    public void deleteComment() {
        actions.hoverElement("button.Delete");
        actions.clickElement("button.Delete");
        actions.isElementPresentUntilTimeout("button.Submit", 5000);
        actions.hoverElement("button.Submit");
        actions.clickElement("button.Submit");
    }

    //--------------ASSERTS-----------------
    public void assertLatestPostsPageNavigated() {
        assertSpecificPageNavigated("posts.url");
    }

    public void assertNewPostPageNavigated() {
        assertSpecificPageNavigated("posts.new");
    }

    public void assertPostCreated() {
        actions.isElementPresentUntilTimeout("posts.CreatedPostLocator", 5000);
        actions.assertElementPresent("posts.CreatedPostLocator");
    }

    public void assertPostIsInLatestPostsPage() {
        actions.isElementPresentUntilTimeout("posts.CreatedPostLocatorInLatestPosts", 5000);
        actions.assertElementPresent("posts.CreatedPostLocatorInLatestPosts");
    }

    public void assertPostFieldsUpdated() {
        actions.isElementPresentUntilTimeout("posts.UpdatedPostLocator", 5000);
        actions.assertElementPresent("posts.UpdatedPostLocator");
    }

    public void assertSearchedPostAppears(String searchWord) {
        Assert.assertNotNull(driver.findElement(By.xpath("//div[@class='row']//h3[contains(text(), \""+Utils.getConfigPropertyByKey(searchWord)+"\")]")));
    }

    public void assertCommentIsAdded() {
        actions.isElementPresentUntilTimeout("comments.CreatedCommentLocator", 5000);
        actions.assertElementPresent("comments.CreatedCommentLocator");
    }

    public void assertCommentIsUpdated() {
        actions.isElementPresentUntilTimeout("comments.UpdatedCommentLocator", 5000);
        actions.assertElementPresent("comments.UpdatedCommentLocator");
    }

    public void assertCommentIsLiked() {
        actions.assertElementPresent("button.DislikeComment");
        actions.waitFor(1500);
    }

    public void assertCommentIsDisliked() {
        actions.assertElementPresent("button.LikeComment");
        actions.waitFor(1500);
    }

    public void assertPostNotPresent() {
        actions.clickElement("navigation.LatestPostsSection");
        actions.waitFor(1500);
        actions.assertElementNotPresent("posts.AssertDeletePost");
    }
}
