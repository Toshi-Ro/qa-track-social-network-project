package com.telerikacademy.finalproject.healthyfoodAPI;

import com.telerikacademy.finalproject.utils.PropertiesManager;
import com.telerikacademy.finalproject.utils.RequestHandler;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class HealthyFoodAPI {

    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }

    public void authenticateDriverForUser(String usernameKey, String passwordKey, WebDriver driver){
        String username = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(encodeValue(usernameKey));
        String password = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(encodeValue(passwordKey));

        // Authenticate and extract cookie
        RequestHandler client = new RequestHandler();
        String requestBody = "username=" + username + "&password=" + password;
        Response response = client.sendPostRequest("https://intense-plains-86846.herokuapp.com/authenticate", requestBody, ContentType.URLENC);
        String cookieValue = response.getDetailedCookie("JSESSIONID").getValue();
        driver.manage().addCookie(new Cookie("JSESSIONID", cookieValue, "intense-plains-86846.herokuapp.com", "/", null, true, true));
    }
}





