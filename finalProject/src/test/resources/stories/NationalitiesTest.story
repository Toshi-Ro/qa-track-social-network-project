Meta:
@endToEnd

Narrative:
As an Admin
I want to add, update and delete a nationality
So that I can manage the existing nationalities list

Scenario:Adding, updating and deleting a nationality as an Admin
Given Click navigation.LoginSection element
When Type admin.Username in login.UsernameField field
And Type admin.Password in login.PasswordField field
And Click button.Submit element

And Hover navigation.SettingsSection element
And Click navigation.NationalitiesSection element
And Click button.Add element
And Type randomName into nationalities.NameField field
And Click button.Submit element

And Locate and click created nationality
And Hover button.Delete element
And Click button.Delete element
And Hover button.Submit element
And Click button.Submit element

Then Click button.LogOut element