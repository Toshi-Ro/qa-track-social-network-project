package stepdefinitions;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.apache.commons.lang.RandomStringUtils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.WebDriver;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();

    private String randomText = RandomStringUtils.randomAlphabetic(8);

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element){
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String field){
        actions.typeValueInField(Utils.getConfigPropertyByKey(value), field);
    }

    @Given("Hover $element element")
    @When("Hover $element element")
    @Then("Hover $element element")
    public void hoverElement(String element){
        actions.hoverElement(element);
    }

    @Given("Type randomName into $name field")
    @When("Type randomName into $name field")
    @Then("Type randomName into $name field")
    public void typeRandomStringInField(String field){
        actions.typeValueInField(randomText, field);
    }

    @Given("Locate and click created nationality")
    @When("Locate and click created nationality")
    @Then("Locate and click created nationality")
    public void clickElement(){
        String specificLocator = "//h3[contains(text(), \""+randomText+"\")]//ancestor::div[3]//a[contains(text(), \"Edit\")]";
        actions.clickElementByXpath(specificLocator);
    }

}
