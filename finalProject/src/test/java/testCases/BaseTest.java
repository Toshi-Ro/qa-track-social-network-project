package testCases;

import com.telerikacademy.finalproject.healthyfoodAPI.HealthyFoodAPI;
import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTest {
	UserActions actions = new UserActions();
	NavigationPage navPage = new NavigationPage();
	HealthyFoodAPI healthyFoodAPI = new HealthyFoodAPI();
	LoginPage loginPage = new LoginPage();
	PostsPage postsPage = new PostsPage();
	CategoriesPage categoriesPage = new CategoriesPage();
	ProfilePage profilePage = new ProfilePage();
	UsersPage usersPage = new UsersPage();
	LogOutPage logOutPage = new LogOutPage();

	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser("base.url");
	}

	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();
	}
}
