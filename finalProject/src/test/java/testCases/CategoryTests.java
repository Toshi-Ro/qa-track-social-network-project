package testCases;

import org.junit.Test;

public class CategoryTests extends BaseTest{

    @Test
    public void addUpdateAndDeleteCategory_asAdmin() {
        //Log in as Admin
        loginPage.navigateToLoginPage();
        loginPage.assertLoginPageNavigated();
        loginPage.logIn("admin.Username", "admin.Password");
        loginPage.assertUserIsLoggedIn();

        //Add new category
        categoriesPage.navigateToCategoriesPage();
        categoriesPage.assertCategoriesPageNavigated();
        categoriesPage.clickAddCategoryButton();
        categoriesPage.assertNewCategoryPageNavigated();
        categoriesPage.populateNewCategoryFields();
        categoriesPage.assertNewCategoryCreated();

        //Update category
        categoriesPage.clickEditCategoryButton();
        categoriesPage.updateCategoryFields();
        categoriesPage.assertCategoryIsUpdated();

        //Delete category
        categoriesPage.deleteCategory();
        categoriesPage.assertCategoryNotPresent();

        //Log out
        logOutPage.logOut();
        loginPage.assertLoginPageNavigated();
    }
}
