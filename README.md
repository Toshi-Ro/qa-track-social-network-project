<h1>Links</h1>

[Environment used for automated tests](https://intense-plains-86846.herokuapp.com/)

[Exploratory testing Report](https://gitlab.com/Toshi-Ro/qa-track-social-network-project/-/blob/master/Exploratory%20Testing%20Report.pdf)

[Test Plan (download)](https://gitlab.com/Toshi-Ro/qa-track-social-network-project/-/blob/master/Test%20Plan.doc)

[Test Report (download)](https://gitlab.com/Toshi-Ro/qa-track-social-network-project/-/blob/master/Test%20Report.xlsx)

[Coverage Report - Manual Test Case Execution](https://gitlab.com/Toshi-Ro/qa-track-social-network-project/-/blob/master/Manual%20Test%20Case%20Execution%20-%20Results%20(25-Nov-2020).pdf)

[GitLab CI .yml control file](https://gitlab.com/Toshi-Ro/qa-track-social-network-project/-/blob/master/.gitlab-ci.yml)

[Alternative link to project (deployed personally)](https://shrouded-wave-54039.herokuapp.com/)

--- 

<h1>Web Service Tests</h1>
<h2>Requirements:</h2>

* Postman v7.36.0 or higher
* Newman v5.2.0 or higher
* Newman Reporter HTMLExtra v1.19.2 or higher

<h2>How to run:</h2>

1. **Run via Postman**

* Download/Pull 'Postman' folder locally
* Open Postman and import "Healthy Food Social Network 5.postman_collection.json" as file
* Run collection via Collection Runner

2. **Run via GitLab CI pipeline + Report (maintainers only)**

* Navigate to project in GitLab
* Navigate to CI/CD -- Pipelines
* Select 'Run pipeline' (with default values)
* Once pipeline run is completed click on 'postman_tests' for results
* Keep or review generated report in 'Job Artifacts'

3. **Run with .bat file + Report**

* Download/Pull 'Postman' folder locally
* Run 'run HFSN5+Report.bat' file
* View report in 'newman' folder in same directory

<h4>Reports Archive:</h4>

[Access here](https://gitlab.com/Aleksandra.Kachakova/final-project-el-dorado-todor-and-aleksandra/-/tree/master/Postman/Postman%20reports%20archive/)

---

<h1>Selenium WebDriver Tests</h1>
<h2>Requirements:</h2>

* JDK v11.0.8
* IntelliJ IDEA 2020.2.3 (Community Edition)
* Apache Maven v3.6.3 or higher

<h2>How to run:</h2>

1. **Run via IntelliJ IDE**

* Download/Pull 'finalProject' folder locally
* Open and build project with IntelliJ IDE
* Navigate to 'src\test\java' and run 'testCases'
* To run **BDD test**: Run 'Runner.java' located in 'src\test\java'

2. **Run with .bat file + Report**

* Download/Pull and open 'finalProject' folder locally
* Run 'Run+report.bat' file
* View report in 'finalProject\target\site\surefire-report.html' (Note: Report is only generated if all tests pass)

---

<h1>Performance testing</h1>
<h2>Requirements:</h2>

* Apache JMeter v5.3

<h2>How to run:</h2>

1. **Run via JMeter**

* Download/Pull 'JMeter Performance Testing' folder locally
* Open JMeter and load 'PerformanceTest.jmx' file
* Start .jmx Test Plan

2. **Run in CMD + HTML Report**

* Download/Pull and open 'finalProject' folder locally
* Open CommandPrompt inside /bin path of JMeter folder on PC
* Type in command: jmeter -n -t [path of .jmx file] -l [path of an empty .csv file] -e -o [path to folder where HTML report will be saved]

<h4>Performance Reports:</h4>

[Access here](https://gitlab.com/Aleksandra.Kachakova/final-project-el-dorado-todor-and-aleksandra/-/tree/master/JMeter%20Performance%20Testing/PERF_TEST_REPORTS)